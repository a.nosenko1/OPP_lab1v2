cmake_minimum_required(VERSION 3.17)
project(lab1v2)

set(CMAKE_CXX_STANDARD 98)
set(CMAKE_CXX_COMPILER "/usr/bin/mpic++")
add_executable(lab1v2 main.cpp)