#include <stdio.h>
#include <stdlib.h>
#include <omp.h>
#include <math.h>
#include <ctime>
#include <iostream>

const int N = 100;
const double
        Pi = 3.14159265358979311600e+00,
        epsilon = 1e-7,
        little_epsilon = 1e-10;

void matrixVectorMul(const int matrixSize, const double *matrix, const double *vector, double *result) {
#pragma omp for
    for (int i = 0; i < matrixSize; ++i) {
        result[i] = 0;
        for (int j = 0; j < matrixSize; ++j) {
            result[i] += *(matrix + matrixSize * i + j) * vector[j];
        }
    }
}

void scalarVectorMul(const int vectorSize, const double scalar, const double *vector, double *result) {
#pragma omp for
    for (int i = 0; i < vectorSize; ++i) {
        result[i] = vector[i] * scalar;
    }
}

void vectorVectorSub(const int vectorSize, const double *vector1, const double *vector2, double *result) {
#pragma omp for
    for (int i = 0; i < vectorSize; ++i) {
        result[i] = vector1[i] - vector2[i];
    }
}

double vectorVectorScalarMul(const int vectorSize, const double *vector1, const double *vector2) {
    double result = 0.0; // 0_0
    #pragma omp shared(result) for reduction(+: result)
    for (int i = 0; i < vectorSize; ++i) {
        result += vector1[i] * vector2[i];
    }
    return result;
}

double vectorLength(const int vectorSize, const double *vector) {
    double result = 0.0;
#pragma omp shared(result) for reduction(+: result)
    for (int i = 0; i < vectorSize; ++i) {
        result += vector[i] * vector[i];
    }
    return (result); //sqrt
}

void copyFirstVectorToSecond(const int vectorSize, const double *vector1, double *vector2) {
#pragma omp for
    for (int i = 0; i < vectorSize; ++i) {
        vector2[i] = vector1[i];
    }
}

void vectorPrint(const int vectorSize, const double *vector) {
    printf("( ");
    for (int i = 0; i < vectorSize; ++i) {
        printf("%3.2f ", *(vector + i));
    }
    printf(")\n");
}

/* Создает случайную симмтеричную матрицу в matrix */
void randomSimMatrixGenerator(const int matrixSize, double *matrix) {
    srand(23);
    for (int i = 0; i < matrixSize; i++) {
        matrix[i * matrixSize + i] = (double) (rand() % 18 - 9); //диагональ
    }
    for (int i = 0; i < matrixSize; i++) {
        for (int j = 0; j < i; j++) {
            matrix[i * matrixSize + j] = (double) (random() % 18 - 9);
            matrix[j * matrixSize + i] = matrix[i * matrixSize + j];
        }
    }
}

void initProisvolDecision(const int matrixSize, double *matrixA, double *vectorB, double *vectorX) {
    double *u = (double *) calloc(matrixSize, sizeof(double));
    randomSimMatrixGenerator(matrixSize, matrixA);
    for (int i = 0; i < matrixSize; ++i) {
        u[i] = sin(2 * Pi / matrixSize);
    }
    matrixVectorMul(matrixSize, matrixA, u, vectorB);
    //vectorPrint(N > 10 ? 10 : N, u); // печать для проверки правильности
    free(u);
}

int main(int argc, char *argv[]) {
    double *A = (double *) calloc(N * N, sizeof(double));
    double *b = (double *) calloc(N, sizeof(double));
    double *x = (double *) calloc(N, sizeof(double));

    initProisvolDecision(N, A, b, x);

    double *Ax = (double *) calloc(N, sizeof(double));
    double *y = (double *) calloc(N, sizeof(double));
    double *Ay = (double *) calloc(N, sizeof(double));
    double *tau_y = (double *) calloc(N, sizeof(double));
    double *x_n_plus_one = (double *) calloc(N, sizeof(double));
    double tempValueForCompare = 1;
    double tau;
    int count = 0;

    struct timespec start, stop;
    clock_gettime(CLOCK_REALTIME, &start);
#pragma omp parallel
    while ((tempValueForCompare - epsilon) > 0) {
#pragma omp single
        count++;

        matrixVectorMul(N, A, x, Ax);  // Ax = A * x;
        vectorVectorSub(N, Ax, b, y);  // y = Ax - b;
        matrixVectorMul(N, A, y, Ay);  // Ay = A * y;

        #pragma omp single
        {
            if (((vectorVectorScalarMul(N, Ay, Ay) - little_epsilon) < 0))
                tau = 0.0;
            else
                tau = vectorVectorScalarMul(N, y, Ay) / vectorVectorScalarMul(N, Ay, Ay);
        };


        scalarVectorMul(N, tau, y, tau_y);           // tau_y = tau * y;
        vectorVectorSub(N, x, tau_y, x_n_plus_one);  // x_n_plus_one = x - tau*y

        #pragma omp single
        {
        if (((vectorLength(N, b) - little_epsilon) < 0))
            tempValueForCompare = 0.0;
        else tempValueForCompare = vectorLength(N, y) / vectorLength(N, b);       // ||Ax - b||/||b|| < E
        };

        copyFirstVectorToSecond(N, x_n_plus_one, x);
    }

    vectorPrint(N > 10 ? 10 : N, x);

    clock_gettime(CLOCK_REALTIME, &stop);
    std::cout << "v2: " << stop.tv_sec - start.tv_sec << std::endl;
    std::cout << count << std::endl;

    free(x);
    free(A);
    free(b);
    free(Ax);
    free(y);
    free(Ay);
    free(tau_y);
    free(x_n_plus_one);

    return 0;
}
