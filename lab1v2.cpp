#include <iostream>
#include <cmath>
#include <malloc.h>
#include <time.h>
#include <stdlib.h>
#include <mpi.h>
using namespace std;
///////////////////////////// GLOBAL PART /////////////////////////////////
const int N = 200;            // размер матрицы
int ProcNum, ProcRank;
int numRowsAtThisProc;      // кол-во строк матрицы в этом процессе
const double Pi = 3.14159265358979311600e+00;
const double epsilon = 1e-5;
const double little_epsilon = 1e-10;
////////////////////////////////////////////////////////////////////////////
void matrixVectorMulParallel(const int matrixSize, const int countRows, const int bufferSize,
                             const double *part_A, double *swap_buffer, double *matrixBuffer,
                             const int *shift, const double *part_vector, double *result);
void matrixVectorMul(const int matrixSize, const double *matrix, const double *vector, double *result);
void scalarVectorMul(const int vectorSize, const double scalar, const double *vector, double *result);
void vectorVectorSub(const int vectorSize, const double *vector1, const double *vector2, double *result);
double vectorVectorScalarMul(const int vectorSize, const double *vector1, const double *vector2);
double vectorLength(const int vectorSize, const double *vector);
void copyFirstVectorToSecond(const int vectorSize, const double *vector1, double *vector2);
void matrixPrint(const int matrixSize, const double *matrix);
void vectorPrint(const int vectorSize, const double *vector);
void randomSimMatrixGenerator(const int matrixSize, double *matrix);
void initProisvolDecision(const int matrixSize, double *matrixA, double *vectorB);
void printResult(const int size, const int count, const double *matrixA, const double *vectorB, const double *vectorX);
///////////////////////////////// MAIN ////////////////////////////////////
int main(int argc,char *argv[]) {
    MPI_Init(&argc,&argv);
    MPI_Comm_size(MPI_COMM_WORLD, &ProcNum);
    MPI_Comm_rank(MPI_COMM_WORLD, &ProcRank);
    ////////////////////////// VARIABLE DECLARATION /////////////////////////////////
    /* Только для 0 процесса: */
    double *A;      // матрица А, будет создана и заполнена только в 0 процессе
    double *b;      // вектор b, будет целиком только в 0 процессе
    double *x;      // вектор x, будет целиком только в 0 процессе
    /* Для каждого процесса: */
    double *part_A; // часть А, содержащая только строки для этого процесса
    double *part_b; // часть b, содержащая только элементы для этого процесса
    double *part_x; // часть x, содержащая только элементы для этого процесса
    /* буфер для части вектора для кольцевого умножения, длины максимального кол-ва строк для процесса */
    double *swapBuffer;
    ///////////////////////// CALCULATION FOR DISTRIBUTION /////////////////////////
    /* В этом разделе будут инициализированы numRowsAtThisProc, countRowsAtProc[], shift[] */

    /* сколько строк дать каждому, максимально поровну */
    int restRows = N;
    for (int i = 0; i < ProcRank; ++i) {
        restRows = restRows - restRows/(ProcNum - i);
    }
    numRowsAtThisProc = restRows / (ProcNum - ProcRank);

    /* массив для содержания кол-ва строк матрицы для каждого процесса */
    /* для i-го процесса countRowsAtProc[i] == кол-ву строк в нем */
    int *countRowsAtProc = new int [ProcNum];   // кол-во строк в i-м процессе
    for (int i = 0; i < ProcNum; ++i) {
        MPI_Sendrecv(&numRowsAtThisProc, 1, MPI_INTEGER, i, 0,
                     (countRowsAtProc+i), 1, MPI_INTEGER, i, 0,
                     MPI_COMM_WORLD, MPI_STATUS_IGNORE);
    }

    /* теперь есть информация для разделения строк по процессам */
    /* вычисляем начальный и конечный номер строки  */
    int startRowPosition = 0;
  //  int stopRowPosition;
    for (int i = 0; i < ProcRank; ++i) {
        startRowPosition += countRowsAtProc[i];
    }
 //   stopRowPosition = startRowPosition + numRowsAtThisProc - 1;
    int *shift = new int [ProcNum];     // сдвиг относительно начала == начальная позиция для i-го процесса
    for (int i = 0; i < ProcNum; ++i) {
        MPI_Sendrecv(&startRowPosition, 1, MPI_INTEGER, i, 0,
                     (shift+i), 1, MPI_INTEGER, i, 0,
                     MPI_COMM_WORLD, MPI_STATUSES_IGNORE);
    }
    /* еще два массива для количества символов и сдвига, но выраженных в кол-ве элементов, а не строк */
    int *countSymAtProc = new int [ProcNum]; // кол-во символов в процессе
    int *shiftMat = new int [ProcNum]; // сдвиг относительно начала в матрице
    for (int i = 0; i < ProcNum; ++i) {
        countSymAtProc[i] = countRowsAtProc[i]*N;
        shiftMat[i] = shift[i]*N;
    }

    /* Найдем, чему равно максимальное и минимальное количество строк для одного процесса */
    /* Для того, чтобы создать буфер такого размера, в котором будет лежать часть вектора для каждого процесса своя */
    int maxPartRowsNum = 0; // размер буфера для умножения
    for (int i = 0; i < ProcNum; ++i) {
        if (countRowsAtProc[i] > maxPartRowsNum)
            maxPartRowsNum = countRowsAtProc[i];
    }

    ///////////////////////////// INIT 0 RANK /////////////////////////////////
    if (ProcRank == 0){
        A = (double*)calloc(N*N, sizeof(double));  // матрица только в нулевом
        b = (double*)calloc(N, sizeof(double)); // полный b в нулевом
        x = (double*)calloc(N, sizeof(double)); // полный x в нулевом
        initProisvolDecision(N, A, b);
        for (int i = 0; i < N; ++i) {
            vectorPrint(N, (A+i*N));
        }
        vectorPrint(N, b);
    }

    ///////////////////////////// DISTRIBUTION ///////////////////////////////
    /* отправим процессам их строки из матрицы А, которая инициализирована в 0 процессе */
    part_A = new double [countRowsAtProc[ProcRank]*N];
    MPI_Scatterv(A, countSymAtProc, shiftMat, MPI_DOUBLE,
                 part_A, countSymAtProc[ProcRank], MPI_DOUBLE, 0, MPI_COMM_WORLD);
    /* отправим роцессам их части вектора b и х */
    part_b = new double [countRowsAtProc[ProcRank]];
    part_x = new double [countRowsAtProc[ProcRank]];
    MPI_Scatterv(b, countRowsAtProc, shift, MPI_DOUBLE,
                 part_b, countRowsAtProc[ProcRank], MPI_DOUBLE, 0, MPI_COMM_WORLD);
    MPI_Scatterv(x, countRowsAtProc, shift, MPI_DOUBLE,
                 part_x, countRowsAtProc[ProcRank], MPI_DOUBLE, 0, MPI_COMM_WORLD);

    swapBuffer = (double *)(calloc(maxPartRowsNum, sizeof(double)));
    int *maxPartRowsNumArray = new int [ProcNum];
    for (int i = 0; i < ProcNum; ++i) {
        maxPartRowsNumArray[i] = maxPartRowsNum;
    }

    ///////////////////////////////////// PREPARE FOR GENERAL EXEC //////////////////

    double* part_Ax = (double*)calloc(countRowsAtProc[ProcRank], sizeof(double));
    double* part_y = (double*)calloc(countRowsAtProc[ProcRank], sizeof(double));
    double* part_Ay = (double*)calloc(countRowsAtProc[ProcRank], sizeof(double));
    double* part_tau_y = (double*)calloc(countRowsAtProc[ProcRank], sizeof(double));
    double* part_x_n_plus_one = (double*)calloc(countRowsAtProc[ProcRank], sizeof(double));

    double tempValueForCompare = 1;
    double tau;
    int count = 0;

    // буфер для части матрицы
    double* matrixBuffer = (double*)calloc(countRowsAtProc[ProcRank]*N, sizeof(double));

    /////////////////////////////////////// GENERAL WHILE ///////////////////////////
    struct timespec start, stop;
    if (ProcRank == 0) clock_gettime(CLOCK_REALTIME, &start);
    while ((tempValueForCompare - epsilon) > 0){
        count++;
        // part_Ax = part_A * part_x;
        matrixVectorMulParallel(N, countRowsAtProc[ProcRank], maxPartRowsNum, part_A, swapBuffer, matrixBuffer, shift, part_x, part_Ax);

        vectorVectorSub(countRowsAtProc[ProcRank], part_Ax, part_b, part_y);  // part_y = part_Ax - part_b;

        // part_Ay = part_A * part_y;
        matrixVectorMulParallel(N, countRowsAtProc[ProcRank], maxPartRowsNum, part_A, swapBuffer, matrixBuffer, shift, part_y, part_Ay);

        double scalarSumPart_AyAy = vectorVectorScalarMul(countRowsAtProc[ProcRank], part_Ay, part_Ay);
        double scalarSumPart_yAy = vectorVectorScalarMul(countRowsAtProc[ProcRank], part_y, part_Ay);
        double AyAy = 0;
        double yAy = 0;
      
        MPI_Allreduce(&scalarSumPart_AyAy, &AyAy, 1, MPI_DOUBLE, MPI_SUM, MPI_COMM_WORLD);
        MPI_Allreduce(&scalarSumPart_yAy, &yAy, 1, MPI_DOUBLE, MPI_SUM, MPI_COMM_WORLD);
      
        if ((AyAy - little_epsilon) < 0)
            tau = 0.0;
        else
            tau = yAy / AyAy;

        // синхронизируем деление
        MPI_Bcast(&tau, 1, MPI_DOUBLE, 0, MPI_COMM_WORLD);

        scalarVectorMul(countRowsAtProc[ProcRank], tau, part_y, part_tau_y);    // part_tau_y = tau * part_y;
        vectorVectorSub(countRowsAtProc[ProcRank], part_x, part_tau_y, part_x_n_plus_one);  // part_x_n_plus_one = part_x - tau*part_y

        double Ax_sub_b_part_2_sum = 0;
        double b_part_2_sum = 0;
        double Ax_sub_b_length;
        double b_length;
        for (int i = 0; i < countRowsAtProc[ProcRank]; ++i) {
            Ax_sub_b_part_2_sum += part_y[i]*part_y[i];
            b_part_2_sum += part_b[i]*part_b[i];
        }
        MPI_Allreduce(&Ax_sub_b_part_2_sum, &Ax_sub_b_length, 1, MPI_DOUBLE, MPI_SUM, MPI_COMM_WORLD);
        MPI_Allreduce(&b_part_2_sum, &b_length, 1, MPI_DOUBLE, MPI_SUM, MPI_COMM_WORLD);
        b_length = sqrt(b_length);
        Ax_sub_b_length = sqrt(Ax_sub_b_length);

        if (((Ax_sub_b_length - little_epsilon) < 0))    // ||part_Ax - b||/||b|| < E
            tempValueForCompare = 0.0;
        else tempValueForCompare = Ax_sub_b_length / b_length;
      
        MPI_Barrier(MPI_COMM_WORLD);
        copyFirstVectorToSecond(countRowsAtProc[ProcRank], part_x_n_plus_one, part_x);
   
        if (count > 1000000) {
            std::cout << "Situatcia help" << std::endl;
            break;
        }
    }
    cout << "ProcRank = " << ProcRank << " Iter = " << count << std::endl;
    if (ProcRank == 0) {
        clock_gettime(CLOCK_REALTIME, &stop);
        std::cout << stop.tv_sec - start.tv_sec << std::endl;
        std::cout << count << std::endl;
    }
    vectorPrint(countRowsAtProc[ProcRank], part_x);
    //if (ProcRank == 0)
      //  MPI_Gatherv(part_x, countRowsAtProc[ProcRank], MPI_DOUBLE, x, countRowsAtProc, shift, MPI_DOUBLE, 0, MPI_COMM_WORLD);
      //  printResult(N, count, A, b, part_x);  // printing result

    free(part_Ax);
    free(part_y);
    free(part_Ay);
    free(part_tau_y);
    free(part_x_n_plus_one);
    free(matrixBuffer);
    if (ProcRank == 0){
        free(A);
        free(b);
        free(x);
    }
    free(part_A);
    free(part_b);
    free(part_x);
    free(swapBuffer);
    delete [] shiftMat;
    delete [] maxPartRowsNumArray;
    delete [] shift;
    delete [] countSymAtProc;
    delete [] countRowsAtProc;
    MPI_Finalize();
    return 0;
}

///////////////////////////// INIT PART /////////////////////////////////
/* Создает случайную симмтеричную матрицу в matrix */
void randomSimMatrixGenerator(const int matrixSize, double *matrix){
    for (int i = 0; i < matrixSize; i++) {
        matrix[i * matrixSize + i] = (double)(rand() % 18 - 9); //диагональ
    }
    for (int i = 0; i < matrixSize; i++) {
        for (int j = 0; j < i; j++) {
            matrix[i * matrixSize + j] = (double)(random() % 18 - 9);
            matrix[j * matrixSize + i] = matrix[i * matrixSize + j];
        }
    }
}
/* Модельная задача с произвольным решением */
void initProisvolDecision(const int matrixSize, double *matrixA, double *vectorB){
    double* u = (double*)calloc(matrixSize, sizeof(double));
    randomSimMatrixGenerator(matrixSize, matrixA);
    for (int i = 0; i < matrixSize; ++i) {
        u[i] = sin(2*Pi/matrixSize);
    }
    matrixVectorMul(matrixSize, matrixA, u, vectorB);
    vectorPrint(matrixSize, u); // печать для проверки правильности
    free(u);
}
///////////////////////////// EXEC PART /////////////////////////////////
void matrixVectorMulParallel(const int matrixSize, const int countRows, const int bufferSize,
                             const double *part_A, double *swap_buffer, double *matrixBuffer,
                             const int *shift, const double *part_vector, double *result){
    /* размер матрицы, количество строк для данного процесса, размер буфер для части вектора для кольцевой передачи,
     * часть матрицы А для данного процесса, буфер для части вектора для кольцевой передачи,
     * буфер для промежуточного результата умножения, массив сдвига, вектор результата.
     * */
    // превратим части вектора в swap_buffer
    for (int i = 0; i < countRows; ++i) {
        swap_buffer[i] = part_vector[i];
    }
    double next_v_first_elem;
    {   // возьмем первый элемент из следующего процесса
        MPI_Request requests[4];
        int prev =  ProcRank - 1;
        int next = ProcRank + 1;
        if(ProcRank == 0) prev = ProcNum - 1;
        if(ProcRank == ProcNum - 1) next = 0;
        MPI_Isend(part_vector, 1, MPI_DOUBLE, prev, 5, MPI_COMM_WORLD, (requests+0));
        MPI_Irecv(&next_v_first_elem, 1, MPI_DOUBLE, next, 5, MPI_COMM_WORLD, (requests+1));
        MPI_Waitall(2, requests, MPI_STATUS_IGNORE);
    }

    int startPos = shift[ProcRank];
    int stopPos = startPos + bufferSize - 1;

    for (int k = 0; k < ProcNum; ++k) {

        for (int i = 0; i < countRows; ++i) {
            for (int j = startPos; j <= stopPos; ++j) {
                matrixBuffer[matrixSize*i + j] = part_A[matrixSize*i + j] * swap_buffer[j - startPos];
            }
        }
        // сначала вышлем индексы предыдущего следующему
        MPI_Request requests[4];
        int prev =  ProcRank - 1;
        int next = ProcRank + 1;
        if(ProcRank == 0) prev = ProcNum - 1;
        if(ProcRank == ProcNum - 1) next = 0;
        MPI_Isend((void *)&startPos, 1, MPI_INT, next, 5, MPI_COMM_WORLD, (requests+0));
        MPI_Irecv((void *)&startPos, 1, MPI_INT, prev, 5, MPI_COMM_WORLD, (requests+1));
        MPI_Waitall(2, requests, MPI_STATUS_IGNORE);
        stopPos = startPos + bufferSize - 1;
        // теперь сдвинем вектор
        MPI_Sendrecv_replace((void *) swap_buffer, bufferSize, MPI_DOUBLE, next, 5,
                             prev, 5, MPI_COMM_WORLD, MPI_STATUS_IGNORE);
        MPI_Barrier(MPI_COMM_WORLD);
    }
    // суммируем строки
    for (int i = 0; i < countRows; ++i) {
        result[i] = 0;
        for (int j = 0; j < matrixSize; ++j) {
            result[i] += matrixBuffer[matrixSize*i + j];
        }
    }
}
///////////////////////////// OTHER /////////////////////////////////////
void matrixVectorMul(const int matrixSize, const double *matrix, const double *vector, double *result){
    for (int i = 0; i < matrixSize; ++i) {
        result[i] = 0;
        for (int j = 0; j < matrixSize; ++j) {
            result[i] += *(matrix+matrixSize*i+j) * vector[j];
        }
    }
}
void scalarVectorMul(const int vectorSize, const double scalar, const double *vector, double *result){
    for (int i = 0; i < vectorSize; ++i) {
        result[i] = vector[i] * scalar;
    }
}
void vectorVectorSub(const int vectorSize, const double *vector1, const double *vector2, double *result){
    for (int i = 0; i < vectorSize; ++i) {
        result[i] = vector1[i] - vector2[i];
    }
}
double vectorVectorScalarMul(const int vectorSize, const double *vector1, const double *vector2){
    double result = 0.0; // 0_0
    for (int i = 0; i < vectorSize; ++i) {
        result += vector1[i] * vector2[i];
    }
    return result;
}
double vectorLength(const int vectorSize, const double *vector){
    double result = 0.0;
    for (int i = 0; i < vectorSize; ++i) {
        result += vector[i]*vector[i];
    }
    return sqrt(result);
}
void copyFirstVectorToSecond(const int vectorSize, const double *vector1, double *vector2){
    for (int i = 0; i < vectorSize; ++i) {
        vector2[i] = vector1[i];
    }
}
void matrixPrint(const int matrixSize, const double *matrix){
    for (int i = 0; i < matrixSize; ++i) {
        printf("    ( ");
        for (int j = 0; j < matrixSize; ++j) {
            printf("%3.2f ", *(matrix+matrixSize*i+j));
        }
        printf(")\n");
    }
}
void vectorPrint(const int vectorSize, const double *vector){
    printf("( ");
    for (int i = 0; i < vectorSize; ++i) {
        printf("%3.2f ", *(vector+i));
    }
    printf(")\n");
}
void printResult(const int size, const int count, const double *matrixA, const double *vectorB, const double *vectorX){
    printf("%s\n", "--------RESULT--------");
    printf("%s %d\n", "iterations count =", count);
    printf("%s", "x = ");
    vectorPrint(size, vectorX); // result
    printf("%s", "b = ");
    vectorPrint(size, vectorB);
    printf("%s\n", "A = ");
    matrixPrint(size, matrixA);
    printf("%s\n", "------RESULT END------");
}
