#include <iostream>
#include <cmath>
#include <malloc.h>
#include <time.h>
#include <stdlib.h>
#include <mpi.h>
using namespace std;
///////////////////////////// GLOBAL PART /////////////////////////////////
const int N = 16;          // размер матрицы
int ProcNum, ProcRank;
const double Pi = 3.14159265358979311600e+00;
const double epsilon = 1e-5;
const double little_epsilon = 1e-10;
////////////////////////////////////////////////////////////////////////////
void matrixVectorMulParallel(double *part_A, double *part_vector, double* result,
                             int matrixSize, int countRows, int bufferSize,
                             double* swap_buffer, double* matrixBuffer,
                             int *startIndexAtProc, int* stopIndexAtProc);
void matrixVectorMul(const int matrixSize, const double *matrix, const double *vector, double *result);
void scalarVectorMul(const int vectorSize, const double scalar, const double *vector, double *result);
void vectorVectorSub(const int vectorSize, const double *vector1, const double *vector2, double *result);
double vectorVectorScalarMul(const int vectorSize, const double *vector1, const double *vector2);
double vectorLength(const int vectorSize, const double *vector);
void copyFirstVectorToSecond(const int vectorSize, const double *vector1, double *vector2);
void matrixPrint(const int matrixSize, const double *matrix);
void vectorPrint(const int vectorSize, const double *vector);
void randomSimMatrixGenerator(const int matrixSize, double *matrix);
void initProisvolDecision(const int matrixSize, double *matrixA, double *vectorB);
void printResult(const int size, const int count, const double *matrixA, const double *vectorB, const double *vectorX);

///////////////////////////////// MAIN ////////////////////////////////////
int main(int argc,char *argv[]) {
    MPI_Init(&argc, &argv);
    MPI_Comm_size(MPI_COMM_WORLD, &ProcNum);
    MPI_Comm_rank(MPI_COMM_WORLD, &ProcRank);
    ////////////////////////// VARIABLE DECLARATION /////////////////////////////////
    /* Только для 0 процесса: */
    double *A;      // матрица А, будет создана и заполнена только в 0 процессе
    double *b;      // вектор b, будет целиком только в 0 процессе
    double *x;      // вектор x, будет целиком только в 0 процессе
    /* Для каждого процесса: */
    double *part_A; // часть А, содержащая только строки для этого процесса
    double *part_b; // часть b, содержащая только элементы для этого процесса
    double *part_x; // часть x, содержащая только элементы для этого процесса
    /* буфер для части вектора для кольцевого умножения */
    double *swap_buffer;
    ///////////////////////// CALCULATION FOR DISTRIBUTION /////////////////////////

    /* массив для того, чтобы хранить информацию о кол-ве строк для каждого процесса */
    int *countRowsAtProc = new int[ProcNum]();   // кол-во строк в i-м процессе

    /* сколько строк дать каждому: делим нацело и остаток первым */
    {
        int countRowsForOneProc = N / ProcNum;
        for (int i = 0; i < ProcNum; ++i) {
            countRowsAtProc[i] = countRowsForOneProc;
        }
        int rest = N % ProcNum;
        int i = 0;
        while (rest) {
            countRowsAtProc[i++] += 1;
            rest--;
        }
    }

    /* массивы (номер процесса | начальный индекс | конечный индекс) */
    int *startIndexAtProc = new int[ProcNum]();
    int *stopIndexAtProc = new int[ProcNum]();
    {
        for (int i = 0; i < ProcNum; ++i) {
            for (int j = 0; j < i; ++j) {
                startIndexAtProc[i] += countRowsAtProc[j];
            }
            stopIndexAtProc[i] = startIndexAtProc[i] + countRowsAtProc[i] - 1;
        }
    }

    /* еще два массива для количества символов и сдвига, но выраженных в кол-ве элементов, а не строк */
    int *countSymAtProc = new int [ProcNum]; // кол-во символов в процессе
    int *startIndexAtProcMulN = new int [ProcNum]; // сдвиг относительно начала в матрице в символах
    for (int i = 0; i < ProcNum; ++i) {
        countSymAtProc[i] = countRowsAtProc[i]*N;
        startIndexAtProcMulN[i] = startIndexAtProc[i] * N;
    }

    /* размер буфера == максимальному числу строк == числу строк 0 процесса из-за распределения остатка */
    int bufferSize = countRowsAtProc[0]; // размер буфера для кольцевого умножения
    // буфер для кольцевой передачи
    swap_buffer = new double [bufferSize]();

    // буфер для части матрицы
    double* matrixBuffer = new double [countRowsAtProc[ProcRank]*N]();

    ///////////////////////////// INIT 0 RANK /////////////////////////////////
    if (ProcRank == 0){
        A = new double [N*N](); // матрица только в нулевом
        b = new double [N]();   // полный b в нулевом
        x = new double [N]();   // полный x в нулевом
        initProisvolDecision(N, A, b);
//        for (int i = 0; i < N; ++i) {
//            vectorPrint(N, (A+i*N));
//        }
//        vectorPrint(N, b);
    }

    ///////////////////////////// DISTRIBUTION ///////////////////////////////
    /* отправим процессам их строки из матрицы А, которая инициализирована в 0 процессе */
    part_A = new double [countRowsAtProc[ProcRank]*N];
    MPI_Scatterv(A, countSymAtProc, startIndexAtProcMulN, MPI_DOUBLE,
                 part_A, countSymAtProc[ProcRank], MPI_DOUBLE, 0, MPI_COMM_WORLD);
    /* отправим процессам их части вектора b и х */
    part_b = new double [countRowsAtProc[ProcRank]];
    part_x = new double [countRowsAtProc[ProcRank]];
    MPI_Scatterv(b, countRowsAtProc, startIndexAtProc, MPI_DOUBLE,
                 part_b, countRowsAtProc[ProcRank], MPI_DOUBLE, 0, MPI_COMM_WORLD);
    MPI_Scatterv(x, countRowsAtProc, startIndexAtProc, MPI_DOUBLE,
                 part_x, countRowsAtProc[ProcRank], MPI_DOUBLE, 0, MPI_COMM_WORLD);
//  //  test
//    for (int i = 0; i < ProcNum; ++i) {
//        MPI_Barrier(MPI_COMM_WORLD);
//        if (i == ProcRank){
//            cout << "ProcRank = " << ProcRank << endl;
//            for (int j = 0; j < countRowsAtProc[ProcRank]; ++j) {
//                vectorPrint(N, (part_A + j*N));
//            }
//        }
//    }

//    matrixVectorMulParallel(N, countRowsAtProc[ProcRank], part_A, part_b, bufferSize, swap_buffer, matrixBuffer,
//                            startIndexAtProc, stopIndexAtProc, part_x);
//    if (ProcRank == 0) {
//        matrixVectorMul(N, A, b, x);
//        vectorPrint(N, x);
//    }

    ///////////////////////////////////// PREPARE FOR GENERAL EXEC //////////////////

    double* part_Ax           = new double[countRowsAtProc[ProcRank]];
    double* part_y            = new double[countRowsAtProc[ProcRank]];
    double* part_Ay           = new double[countRowsAtProc[ProcRank]];
    double* part_tau_y        = new double[countRowsAtProc[ProcRank]];
    double* part_x_n_plus_one = new double[countRowsAtProc[ProcRank]];

    double tempValueForCompare = 1;
    double tau;
    int count = 0;

    /////////////////////////////////////// GENERAL WHILE ///////////////////////////
    struct timespec start, stop;
    if (ProcRank == 0) clock_gettime(CLOCK_REALTIME, &start);
    while ((tempValueForCompare - epsilon) > 0){
        count++;
        // part_Ax = part_A * part_x;
        matrixVectorMulParallel(part_A, part_x, part_Ax,
                                N, countRowsAtProc[ProcRank], bufferSize,
                                swap_buffer, matrixBuffer,
                                startIndexAtProc, stopIndexAtProc);
//        cout << "ProcRank = " << ProcRank << " Iter = " << count << " Ax: ";
//        vectorPrint(countRowsAtProc[ProcRank], part_Ax);

        vectorVectorSub(countRowsAtProc[ProcRank], part_Ax, part_b, part_y);  // part_y = part_Ax - part_b;

        // part_Ay = part_A * part_y;
        matrixVectorMulParallel(part_A, part_y, part_Ay,
                                N, countRowsAtProc[ProcRank], bufferSize,
                                swap_buffer, matrixBuffer,
                                startIndexAtProc, stopIndexAtProc);

        double scalarSumPart_AyAy = vectorVectorScalarMul(countRowsAtProc[ProcRank], part_Ay, part_Ay);
        double scalarSumPart_yAy = vectorVectorScalarMul(countRowsAtProc[ProcRank], part_y, part_Ay);
        double AyAy = 0;
        double yAy = 0;
        //  cout << "ProcRank = " << ProcRank << " Iter = " << count << " scalarSumPart_AyAy = " << scalarSumPart_AyAy << endl;
        MPI_Allreduce(&scalarSumPart_AyAy, &AyAy, 1, MPI_DOUBLE, MPI_SUM, MPI_COMM_WORLD);
        MPI_Allreduce(&scalarSumPart_yAy, &yAy, 1, MPI_DOUBLE, MPI_SUM, MPI_COMM_WORLD);
        //  cout << "ProcRank = " << ProcRank << " Iter = " << count << " AyAy = " << AyAy << endl;
        if ((AyAy - little_epsilon) < 0)
            tau = 0.0;
        else
            tau = yAy / AyAy;

        // синхронизируем деление
        MPI_Bcast(&tau, 1, MPI_DOUBLE, 0, MPI_COMM_WORLD);

        scalarVectorMul(countRowsAtProc[ProcRank], tau, part_y, part_tau_y);    // part_tau_y = tau * part_y;
        vectorVectorSub(countRowsAtProc[ProcRank], part_x, part_tau_y, part_x_n_plus_one);  // part_x_n_plus_one = part_x - tau*part_y

        double Ax_sub_b_part_2_sum = 0;
        double b_part_2_sum = 0;
        double Ax_sub_b_length;
        double b_length;
        for (int i = 0; i < countRowsAtProc[ProcRank]; ++i) {
            Ax_sub_b_part_2_sum += part_y[i]*part_y[i];
            b_part_2_sum += part_b[i]*part_b[i];
        }
        MPI_Allreduce(&Ax_sub_b_part_2_sum, &Ax_sub_b_length, 1, MPI_DOUBLE, MPI_SUM, MPI_COMM_WORLD);
        MPI_Allreduce(&b_part_2_sum, &b_length, 1, MPI_DOUBLE, MPI_SUM, MPI_COMM_WORLD);
        b_length = sqrt(b_length);
        Ax_sub_b_length = sqrt(Ax_sub_b_length);

        if (((Ax_sub_b_length - little_epsilon) < 0))    // ||part_Ax - b||/||b|| < E
            tempValueForCompare = 0.0;
        else tempValueForCompare = Ax_sub_b_length / b_length;
        //  cout << tempValueForCompare << endl;
        MPI_Barrier(MPI_COMM_WORLD);
        copyFirstVectorToSecond(countRowsAtProc[ProcRank], part_x_n_plus_one, part_x);
        // cout << "ProcRank = " << ProcRank << " Iter = " << count << " part_x: ";
        //  vectorPrint(countRowsAtProc[ProcRank], part_x);
        if (count > 1000000) {
            std::cout << "Situatcia help" << std::endl;
            break;
        }
    }
    //   cout << "ProcRank = " << ProcRank << " Iter = " << count << std::endl;
    if (ProcRank == 0) {
        clock_gettime(CLOCK_REALTIME, &stop);
        std::cout << stop.tv_sec - start.tv_sec << std::endl;
        std::cout << count << std::endl;
    }
    // вывод х для проверки
//    for (int i = 0; i < ProcNum; ++i) {
//        MPI_Barrier(MPI_COMM_WORLD);
//        if (i == ProcRank) {
//            cout << "ProcRank = " << ProcRank << endl;
//            vectorPrint(countRowsAtProc[ProcRank], part_x);
//        }
//    }

    delete[] part_Ax;
    delete[] part_y;
    delete[] part_Ay;
    delete[] part_tau_y;
    delete[] part_x_n_plus_one;
    delete[] matrixBuffer;
    if (ProcRank == 0){
        delete[] A;
        delete[] b;
        delete[] x;
    }
    delete[] part_A;
    delete[] part_b;
    delete[] part_x;
    delete[] swap_buffer;
    delete [] countSymAtProc;
    delete [] countRowsAtProc;
    delete [] startIndexAtProc;
    delete [] stopIndexAtProc;
    delete [] startIndexAtProcMulN;
    MPI_Finalize();
    return 0;
}

///////////////////////////// EXEC PART /////////////////////////////////
void matrixVectorMulParallel(double *part_A, double *part_vector, double* result,
                             int matrixSize, int countRows, int bufferSize,
                             double* swap_buffer, double* matrixBuffer,
                             int *startIndexAtProc, int* stopIndexAtProc){

    for (int i = 0; i < countRows; ++i) {
        swap_buffer[i] = part_vector[i];
    }

    int prev =  ProcRank - 1;
    int next = ProcRank + 1;
    if(ProcRank == 0) prev = ProcNum - 1;
    if(ProcRank == ProcNum - 1) next = 0;

    for (int i = 0; i < ProcNum; ++i) {
        int startPos = startIndexAtProc[(ProcRank + i)%ProcNum];
        int stopPos = stopIndexAtProc[(ProcRank + i)%ProcNum];
        for (int j = 0; j < countRows; ++j) {
            for (int k = startPos; k <= stopPos; ++k) {
                matrixBuffer[matrixSize * j + k] = part_A[matrixSize * j + k] * swap_buffer[k - startPos];
            }
        }
        MPI_Sendrecv_replace((void *) swap_buffer, bufferSize, MPI_DOUBLE, prev, 5,
                             next, 5, MPI_COMM_WORLD, MPI_STATUS_IGNORE);
    }

    // суммируем строки
    for (int i = 0; i < countRows; ++i) {
        result[i] = 0;
        for (int j = 0; j < matrixSize; ++j) {
            result[i] += matrixBuffer[matrixSize*i + j];
        }
    }
//    MPI_Barrier(MPI_COMM_WORLD);
//    for (int i = 0; i < ProcNum; ++i) {
//        MPI_Barrier(MPI_COMM_WORLD);
//        if (i == ProcRank){
//            cout << "ProcRank = " << ProcRank << endl;
//            for (int j = 0; j < countRows; ++j) {
//                cout << result[j] << endl;
//            }
//        }
//    }
}

///////////////////////////// INIT PART /////////////////////////////////
/* Создает случайную симмтеричную матрицу в matrix */
void randomSimMatrixGenerator(const int matrixSize, double *matrix){
    srand(1);
    for (int i = 0; i < matrixSize; i++) {
        matrix[i * matrixSize + i] = (double)(rand() % 18 - 9); //диагональ
    }
    for (int i = 0; i < matrixSize; i++) {
        for (int j = 0; j < i; j++) {
            matrix[i * matrixSize + j] = (double)(random() % 18 - 9);
            matrix[j * matrixSize + i] = matrix[i * matrixSize + j];
        }
    }
}
/* Модельная задача с произвольным решением */
void initProisvolDecision(const int matrixSize, double *matrixA, double *vectorB){
    double* u = (double*)calloc(matrixSize, sizeof(double));
    randomSimMatrixGenerator(matrixSize, matrixA);
    for (int i = 0; i < matrixSize; ++i) {
        u[i] = sin(2*Pi/matrixSize);
    }
    matrixVectorMul(matrixSize, matrixA, u, vectorB);
    //vectorPrint(matrixSize, u); // печать для проверки правильности
    free(u);
}
///////////////////////////// OTHER /////////////////////////////////////
void matrixVectorMul(const int matrixSize, const double *matrix, const double *vector, double *result){
    for (int i = 0; i < matrixSize; ++i) {
        result[i] = 0;
        for (int j = 0; j < matrixSize; ++j) {
            result[i] += *(matrix+matrixSize*i+j) * vector[j];
        }
    }
}
void scalarVectorMul(const int vectorSize, const double scalar, const double *vector, double *result){
    for (int i = 0; i < vectorSize; ++i) {
        result[i] = vector[i] * scalar;
    }
}
void vectorVectorSub(const int vectorSize, const double *vector1, const double *vector2, double *result){
    for (int i = 0; i < vectorSize; ++i) {
        result[i] = vector1[i] - vector2[i];
    }
}
double vectorVectorScalarMul(const int vectorSize, const double *vector1, const double *vector2){
    double result = 0.0; // 0_0
    for (int i = 0; i < vectorSize; ++i) {
        result += vector1[i] * vector2[i];
    }
    return result;
}
double vectorLength(const int vectorSize, const double *vector){
    double result = 0.0;
    for (int i = 0; i < vectorSize; ++i) {
        result += vector[i]*vector[i];
    }
    return sqrt(result);
}
void copyFirstVectorToSecond(const int vectorSize, const double *vector1, double *vector2){
    for (int i = 0; i < vectorSize; ++i) {
        vector2[i] = vector1[i];
    }
}
void matrixPrint(const int matrixSize, const double *matrix){
    for (int i = 0; i < matrixSize; ++i) {
        printf("    ( ");
        for (int j = 0; j < matrixSize; ++j) {
            printf("%3.2f ", *(matrix+matrixSize*i+j));
        }
        printf(")\n");
    }
}
void vectorPrint(const int vectorSize, const double *vector){
    printf("( ");
    for (int i = 0; i < vectorSize; ++i) {
        printf("%3.2f ", *(vector+i));
    }
    printf(")\n");
}
void printResult(const int size, const int count, const double *matrixA, const double *vectorB, const double *vectorX){
    printf("%s\n", "--------RESULT--------");
    printf("%s %d\n", "iterations count =", count);
    printf("%s", "x = ");
    vectorPrint(size, vectorX); // result
    printf("%s", "b = ");
    vectorPrint(size, vectorB);
    printf("%s\n", "A = ");
    matrixPrint(size, matrixA);
    printf("%s\n", "------RESULT END------");
}
